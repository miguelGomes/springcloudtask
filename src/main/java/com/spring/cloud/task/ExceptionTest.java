package com.spring.cloud.task;

/**
 * Created by MiguelGomes on 8/5/16.
 */
public class ExceptionTest extends Exception {

    public ExceptionTest(String text) {
        super(text);
    }

}
