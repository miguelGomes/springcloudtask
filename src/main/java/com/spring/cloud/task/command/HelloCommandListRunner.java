package com.spring.cloud.task.command;

import com.spring.cloud.task.ExceptionTest;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.ExitCodeExceptionMapper;

/**
 * Created by MiguelGomes on 8/5/16.
 */
public class HelloCommandListRunner implements CommandLineRunner, ExitCodeExceptionMapper {

    @Override
    public void run(String... args) throws Exception {
        throw new ExceptionTest("test");
    }

    @Override
    public int getExitCode(Throwable exception) {

        if (exception.getCause() instanceof ExceptionTest) {
            return 10;
        }

        return 0;
    }
}
