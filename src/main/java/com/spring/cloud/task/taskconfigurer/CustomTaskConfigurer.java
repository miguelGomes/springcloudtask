package com.spring.cloud.task.taskconfigurer;

import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.cloud.task.configuration.TaskConfigurer;
import org.springframework.cloud.task.repository.TaskExplorer;
import org.springframework.cloud.task.repository.TaskRepository;
import org.springframework.cloud.task.repository.support.SimpleTaskExplorer;
import org.springframework.cloud.task.repository.support.SimpleTaskRepository;
import org.springframework.cloud.task.repository.support.TaskExecutionDaoFactoryBean;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Created by MiguelGomes on 8/6/16.
 */
@Component
public class CustomTaskConfigurer implements TaskConfigurer {

    private TaskRepository taskRepository;

    private TaskExplorer taskExplorer;

    {
        TaskExecutionDaoFactoryBean taskExecutionDaoFactoryBean = new TaskExecutionDaoFactoryBean();

        this.taskRepository = new SimpleTaskRepository(taskExecutionDaoFactoryBean);
        this.taskExplorer = new SimpleTaskExplorer(taskExecutionDaoFactoryBean);
    }

    @Override
    public TaskRepository getTaskRepository() {
        return this.taskRepository;
    }

    @Override
    public PlatformTransactionManager getTransactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Override
    public TaskExplorer getTaskExplorer() {
        return this.taskExplorer;
    }
}
