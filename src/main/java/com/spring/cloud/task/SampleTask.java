package com.spring.cloud.task;

import com.spring.cloud.task.command.HelloCommandListRunner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.cloud.task.listener.annotation.AfterTask;
import org.springframework.cloud.task.listener.annotation.BeforeTask;
import org.springframework.cloud.task.listener.annotation.FailedTask;
import org.springframework.cloud.task.repository.TaskExecution;
import org.springframework.context.annotation.Bean;

/**
 * Created by MiguelGomes on 8/5/16.
 */
@SpringBootApplication
@EnableTask
public class SampleTask {

    private static final Logger LOGGER = LoggerFactory.getLogger(SampleTask.class);

    @Bean
    public CommandLineRunner commandLineRunner(){
        return new HelloCommandListRunner();
    }

    public static void main(String[] args) {
        SpringApplication.run(SampleTask.class, args);
    }

    @BeforeTask
    public void onTaskStartup(TaskExecution taskExecution) {
        LOGGER.info("BeforeTask");
    }

    @AfterTask
    public void onTaskEnd(TaskExecution taskExecution) {
        LOGGER.info("AfterTask");
    }

    @FailedTask
    public void onTaskFailed(TaskExecution taskExecution) {
        LOGGER.info("FailedTask");
    }

}
